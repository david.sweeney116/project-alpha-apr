from django.shortcuts import render, redirect
from tasks.forms import createForm
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = createForm(request.POST)
        if form.is_valid():
            tasks = form.save()
            tasks.purchaser = request.user
            tasks.save()
            return redirect("list_projects")
    else:
        form = createForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)
