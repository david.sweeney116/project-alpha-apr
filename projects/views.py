from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import createForm


@login_required
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    context = {
        "projects": project,
    }
    return render(request, "projects/List.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = createForm(request.POST)
        if form.is_valid():
            Project = form.save(False)
            Project.purchaser = request.user
            Project.save()
            return redirect("list_projects")
    else:
        form = createForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)


@login_required
def show_project(request, id):
    task = get_object_or_404(Task, id=id)
    context = {
        "tasks": task,
    }
    return render(request, "projects/detail.html", context)
